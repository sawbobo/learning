Oracle DBA 11g/12c

Role of a DBA:
-Installation, configuration, upgrade and migration
-Backup and recovery
-Database security
-Storage and capacity planing
-Performance monitoring and tuning
-Troubleshooting
-High availability

---------------------------------------
OLTP Vs OLAP
OLTP - Online Transaction Processing
-Current Data
-Constantly Updated
-Highly detailed data
-Day to day operations
-Normalized design

OLAP - Online Analytical Processing
-Historical Data
-Less frequently updated
-Summarized data
-Reports/Dashboards for decision making
-De-normalized design

----------------------------------
Oracle 11g (g - Grid)
Grid Computing is a way of organizing computer resources in such a way that they can be flexible and dynamically allocated and accessed.
-Processors
-Storage
-Databases
-Applications

Oracle 12c (c - Cloud)
Oracle 12c using Container Database(CDB)

-------------------------------------
Data Block
-A data block is the smallext unit of storage in an Oracle databae.
-Oracle stores data in data blocks(also called logical blocks, Oracle blocks, or pages)
-One data block corresponds to a specific number of bytes of physical database space on disk.
-Default block size in 11g is 8192 bytes(8kb)

Data Movement
CPU --> Primary Memory (Executable Memory/Temporary Memory) --> Secondary Memory (Permanent/Persistent Mermory)

-------------------------------------
Undo vs Redo Data
Undo means to reverse the action of an earlier action.
-Oracle records all data that is about to change as undo. The information allows Oracle to undo changes in case of a rollback.

Redo means to do something again.
-Oracle records all changes made to the database. If the database crashes, this information allows Oracle to redo(reprocess) the changes.

---------------------------------------
Oracle Instance vs Database

The Instance refers to the set of Oracle background processes or threads and a shared memory area(SGA).
Every running Oracle database is associated with (at least) an Oracle instance.

The Database is a set of physical operating system files. These files actually holds the user data and the metada (or the data dictionary) and other information about the database.

RDBMS
    -Instance
        -Processes
            -PMON
            -SMON
            -DBWn
            -Ckpt
            -LGWR
            -ARCn
        -Memory Structures
            -SGA
                -Data Buffer Cache
                -Shared Pool
                -Redo Log Buffer
                -Large Pool
                -Java Pool
                -Streams Pool
            -PGA
    -Database
        -Data files
        -Redo Log files
        -Archived Redo Log files
        -Control Files
        -Parameter File
        -Password File
        -Backup Files
        -Trace Files
        -Alert log file

Real Application Clusters(RAC)
The database must be associated with at least one instance minimum but the database can connected to more instances if necessary.
-Database (Instance1, Instance2, Instance3)

----------------------------------------------------
Process Architecture

Listener helping in communicating between the server process and user process. The user process connect to the listener. The listener connect to the server process and then established a connection, once the connection is established now the server process and the user process communicates directly. The server process has its own PGA. The server process connected to Instance.

The server process is doing:
-Parse and run SQL statements
-Read data blocks from data files on disk and store in shared database buffer cache
-Return results in a way the user application understands

------------------------------------------------------
Log Writer Process
Writes the redo log buffer to a redo log file on disk.
It will write:
-When a user process commits a transaction
-When the redo log buffer is one-third full
-Before a DBWn process writes modified buffers to disk
-Every 3 seconds

[Redo log buffer] --> [LGWR] --> [REDO log files]

------------------------------------------------------
Checkpoint Process
A Checkpoint is a database event which synchronizes the modified data blocks in memory with the data files on disk.
It also updates the datafile headers and control file with the latest checkpoint System Change Number(SCN).
-Establish data consistency
-Enable faster database recovery

A Checkpoint will occur:
-At each switch of the redo log files.
-Once the number of seconds defined in the LOG_CHECKPOINT_TIMEOUT is reached.
-Once the current redo log file reaches the size:
\LOG_CHECKPOINT_INTERVAL * size of IO OS blocks\
\Eg: 10000 * 512 = 5120000 (5M)\
-Directly by the ALTER SYSTEM SWITCH LOGFILE command.
-Directly with the ALTER SYSTEM CHECKPOINT command.

Records checkpoint information (SCN) into the Control file and each data file header.

[CKPT] --> [Data Files, Control File]

-------------------------------------------------------
System Monitor Process (SMON)
-Performs recovery at instance startup
-Cleans up unused tempoary segments

[SMON]

--------------------------------------------------------
Process Monitor Process (PMON)
-Performs process recovery when a user process fails
    -Cleans up the database buffer cache
    -Frees resources that are used by the user process
-Monitors sessions for idel session timeout
-Restarts stopped running dispatchers and server processes
-Dynamically registers database services with network listeners

[PMON]

-------------------------------------------------------
Recoverer Process (RECO)
-Used with the distributed database configuration
-Automactically connects to other database involved in in-doubt distributed transactions
-Automactically resolves all in-doubt transactions
-Removes any rows that correspond to in-doubt transactions

[RECO]

-------------------------------------------------------
Archiver Process (ARC)
-Copy redo log files to a designated storage device after a log switch has occurred
-Can collect transaction redo data and transmit that data to standby destinations
-Runs only in ARCHIVELOG mode
-LOG_ARCHIVE_MAX_PROCESSES

[REDO Log Files] --> [ARCn] --> Storage

-------------------------------------------------------
Database Buffer Cache
-Database buffer cache holds copies of data blocks that are read from data files
-It is shared by all concurrent users
-Number of buffers defined by DB_BLOCK_BUFFERS
-Size of a buffer based on DB_BLOCK_SIZE
-Stores the most recently used blocks

-------------------------------------------------------
Shared Pool (Library Cache, Data Dictionary Cache, Results Cache)
-Livrary Cache contains statement text, parsed code, and an execution plan
-A Shared SQL area contains the parse tree and execution plan for a given SQL statement
-Data Dictionary Cache contains table and column definitions and privileges
-The size of the shared pool is defined by the initialization parameter SHARED_POOL_SIZE in the parameter file

--------------------------------------------------------
Redo Log Buffer
-Holds information about changes made to the database
-Contains redo entries that have the information to redo changes made by operations such as DML and DDL
-Circular Buffer
-Size defined by LOG_BUFFER

--------------------------------------------------------
Large Pool (Optional)
DBA can configure an optional memory area called the large pool to provide large memory allocations for:
-Session memory for the shared server and the Oracle XA interface(used where transactions interact with multiple databases)
-I/O server processes
-Oracle Database backup and restore operations
-Parallel Query operations
-Advanced Queuing memory table storage
-Size defined by LARGE_POOL_SIZE

---------------------------------------------------------
Java Pool & Streams Pool
-Java Pool: Java pool memory is used to store all session-specific Java code and data in the JVM. Size defined by JAVA_POLL_SIZE
-Streams Pool: Streams pool memory is used exclusively by Oracle Streams to store buffered queue messages and provide memory for Oracle Streams processes. Size defined by STREAMS_POOL_SIZE

----------------------------------------------------------
Keep Buffer Pool
-Data which is frequently accessed can be pinned in the Keep buffer pool
-Keep buffer pool retains data in the memory. So that next request for same data can be entertained from memory
-This avoids disk read and increases performance. Usually small objects should be kept in Keep buffer
-DB_KEEP_CACHE_SIZE initialization parameter is used to create Keep buffer pool

-----------------------------------------------------------
Recycle Buffer Pool
Recycle pool is reserved for large tables that experience full table scans that are unlikely to be reread or accessed rarely. The Recycle pool is used so that the incoming data blocks don't flush out data blocks from more frequently used tables and indexes. Size defined by DB_RECYCLE_CACHE_SIZE

-------------------------------------------------------------
nK Buffer Cache
nK Buffer Cache is use to store oracle data blocks having different size then the default block size. nK Buffer Cache is used hold blocks of size = nK where n = 2,4,8,16,32 where K is equal to the size of the default block (DB_BLOCK_SIZE). Size defined by DB_NK_CACHE_SIZE
DB_2K_CACHE_SIZE = 0M
DB_4K_CACHE_SIZE = 0M

------------------------------------------------------------
Program Global Area (PGA)
The Program Global Area (PGA) is a private memory region containing data and control information for a server process. Each server process has a distinct PGA. Access to it is exclusive to that the server process is read only by Oracle code acting on behalf of it. It is not available for developer's code. The PGA is allocated when a process is created and deallocated wher the process is terminated.
-User Global Area
    -Stack Space
    -Cursor State
    -User Session Data
    -Sort Area
    -Hash Area
    -Create Bitmap Area
    -Bitmap Merge Area

PGA in a Shared Environment
In a shared server environment, multiple client users share the server process. In this model, the UGA is moved into the SGA (shared pool or large pool if configured) leaving the PGA with only stack space.

--------------------------------------------------------------
Dedicated Vs Shared Servers
Dedicated Server: One server process is dedicated to each database connection
Shared Servers: Server processes are shared amond many database connections

--------------------------------------------------------------
Transaction Example - UPDATE
UPDATE CUSTOMER SET name = 'JOHN' WHERE cust_id = '12345'

1.Connection to server is established, PGA is allocated, Statement is sent to server
2.Server checks the shared SQL pool for a parsed statement, Statement is parsed and put in shared SQL area
3.Statement is executed, Server reads data from data files into database buffer cache
4.Server makes the change to the block in the database buffer cache
5.Change to database buffer cache(after change occurs) written to redo log buffer
6.Undo information(before change occurs) recorded in rollback segment
7.Change to rollback segment recorded in the redo log buffer
8.Update confirmation message disployed to user
9.COMMIT statement issued by user
10.LGWR writes the redo information to the online redo logs, 'FAST' COMMIT
11.Successful commit message
12.DBWR writes changed block to the datafile

------------------------------------------------------------
Oracle software installation
-Create /u01
-Create user 'oracle'
-Create group 'oinstall'
-Grant owner oracle:oinstall on /u01 /opt /disk1
-Grant permission 751 on /u01
-Modify ~/.bash_profile
    -export ORACLE_SID={SID}
    -export ORACLE_BASE=/u01/app/oracle
    -export ORACLE_HOME=/u01/app/oracle/product/{version}
    -export PATH=$ORACLE_HOME/bin:$PATH
-Run '. .bash_profile'
-Run 'xhost +' as root
-Run runInstaller

--------------------------------------------------------------
Oracle Parameter File
Parameter file is a text/binary file used to store the database initialization parameters. The oracle instance reads the parameter file during startup which are then used to control the behavior of database instance. It is also used to specify options like:
    -memory allocation (SGA and PGA)
    -startup of optional background processes
    -Setting of NLS parameters etc
There are 2 types of parameter files:
    1.pfile (parameter file) - older way [not recommended by oracle]
    2.spfile (server parameter file) - newer way [recommended by oracle]
spfile was introduced starting from oracle 9i, until that time text based pfile was used to store database initialization parameters.

-PFILE
    -Text file
    -Parameters in pfile can be edited using any text editor
    -Default location of pfile - $ORACLE_HOME/dbs/init{SID}.ora where {SID} is the name of the instance
    -The RMAN utility can not take backup of a pfile
-SPFILE
    -Binary file
    -spfile cannot be edited using a text editor. Instead it can only be altered using the "ALTER SYSTEM" command
    -The RMAN utility can take backup of spfile
How to check if SPFILE or PFILE is used? Run the blow command:
SQL>show parameter spfile
If the query returns no rows - pfile is used.

---------------------------------------------------------------
Database Creation - Manual Method
-Create folder /disk1/{SID}/data,redo,control,log,arch,diag,fra
-Create init{SID}.ora under $ORACLE_HOME/dbs/ for parameter file. Add following data in ora file:
    -db_name={SID}
    -memory_target=500m
    -control_files='/disk1/{SID}/control/control01.ctl','/disk1/{SID}/control/control02.ctl'
    -diagnostic_dest='/disk1/{SID}/diag'
    -compatible={version}
-Create dbcreate.sql under $ORACLE_HOME/dbs/ for db creation. Add following data in sql file:
    -create database {SID}
    -datafile '/disk1/{SID}/data/system.dbf' size 300m autoextend on
    -sysaux datafile '/disk1/{SID}/data/sysaux.dbf' size 300m autoextend on
    -default tablespace user_data datafile '/disk1/{SID}/data/user01.dbf' size 500m
    -undo tablespace undotbs datafile '/disk1/{SID}/data/undotbs.dbf' size 100m
    -logfile group 1 '/disk1/{SID}/redo/redo1.rdo' size 100m,
             group 2 '/disk1/{SID}/redo/redo2.rdo' size 100m;
-export ORACLE_SID={SID}
-sqlplus / as sysdba
    SQL> startup nomount
    SQL> @//u01/app/oracle/product/{version}/dbs/dbcreat.sql
    SQL> @$ORACLE_HOME/rdbms/admin/catalog.sql
    SQL> @$ORACLE_HOME/rdbms/admin/catproc.sql
    SQL> @$ORACLE_HOME/sqlplus/admin/pupbld.sql
    SQL> shut immediate
    SQL> exit

-sqlplus / as sysdba  (to verify database creation)
    SQL> startup
    SQL> select name from v$database;
    SQL> select name from v$controlfile;
    SQL> select name from v$datafile;
    SQL> select * from dual;
    SQL> select sysdate from dual;
    SQL> exit

-----------------------------------------------------------------
Database Creation - Using DBCA
-Run 'xhost +' as root
-Create folder /disk1/{SID}/data,redo,control,log,arch,diag,fra
-Run $ORACLE_HOME/dbca
-Select '/disk1/{SID}/fra' as Fast Recovery Area
-Select Character Sets and Connection Mode
-Mention Controlfile, Datafiles and Redo_Log location '/disk1/{SID}/control,data/log'

-----------------------------------------------------------------






