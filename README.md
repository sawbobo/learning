# Kubernetes Architecture
## Master Node (Manage, Plan, Schedule, Monitor Nodes)
- ETCD Cluster
- Kube-apiserver
- Kube Controller Manager
- Kube-scheduler

## Worker Nodes (Host Application as Containers)
- kubelet
- Kube-proxy
- Container Runtime Engine (docker, rkt)

### ETCD
ETCD is a distributed reliable key value store that is Simple, Secure Fast

Install ETCD
1. Download Binaries
curl -L https://github.com/etcd io/etcd/releases/download/v3.3.11/etcd-v3.3.11-linux-amd64.tar.gz -o etcd-v3.3.11-linux-amd64.tar.gz
2. Extract
tar xzvf etcd-v3.3.11-linux-amd64.tar.gz
3. Run ETCD Service
./etcd    (listen on port 2379)

Operate ETCD
./etcdctl set key1 value1
./etcdctl get key1
./etcdctl

ETCD in kubernetes store information regarding cluster such as:
- Nodes
- PODs
- Configs
- Secrets
- Accounts
- Roles
- Bindings
- Others

Explore ETCD (Run inside the etcd-master POD)
kubectl exec etcd-master -n kube-system etcdctl get / --prefix -keys-only

Kubernetes store data in the specific directory stracture. Root directory is "Regisitry".
 /registry/minions
 /registry/pods
 /registry/replicasets
 /registry/deployments
 /registry/roles
 /registry/secrets
